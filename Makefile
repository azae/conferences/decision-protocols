TEXFILES = $(wildcard *.tex)
SVGFILES = $(wildcard images/*.svg)

.PHONY: all pdf clean img

all: pdf

clean:
	@latexmk -C -silent
	@rm -rf _minted-*

pdf: img $(patsubst %.tex,%.pdf,$(TEXFILES))

img: $(patsubst %.svg,%.pdf,$(SVGFILES))

%.pdf: %.tex
	latexmk $<

%.pdf: %.svg
	inkscape -f $< --export-pdf=$@

#find . -name "*.tex" -o -path "./code/*" -o -path "./style/*" |entr make clean pdf
